import cv2
import dlib
import numpy
import RPi.GPIO as gpio
from time import time, sleep
import os
import requests
import socket
import io
import pickle
from datetime import datetime
from base64 import b64encode
import smtplib
import ssl
import slack


def fstr(float):
    return '{:.2}'.format(float)


# comunicação socket foi feita de forma rudimentar, tendo a conexão encerrada a cada chamada da função
class socketo():
    def get(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((socket.gethostname(), 1753))
        
        stream = io.BytesIO()
        ti = time()
        while True:
            msg = self.sock.recv(1024)
            if len(msg) <= 0:
                break
            stream.write(msg)
        
        face = pickle.loads(stream.getvalue())
        tf = time()
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

        return face


class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('./recognition.dat')
        self.shape_predictor = dlib.shape_predictor('./shape.dat')
        self.faces = []
        self.is_known = False
        self.name = ''
        
    def gen_face_desc(self, face_image, face_bounds):
        # define os 68 pontos de referência na face
        face_landmarks = self.shape_predictor(face_image, face_bounds)
        # redimensiona, recorta e alinha a face
        face_chip = dlib.get_face_chip(face_image, face_landmarks
        # calcula o descritor de face (vetor de 128 posições)
        face_desc = numpy.array(self.recognition_model.compute_face_descriptor(face_chip, 1))
        return face_desc
        
    def add_faces(self, new_faces):
        for face in new_faces:
            face['desc'] = numpy.array(face['desc'])
        self.faces.extend(new_faces)
        print('[INFO] Adicao de faces realizada')
    
    def rem_faces(self, names):
        for name in names:
            i = 0
            for face in self.faces:
                if face['name'] == name:
                    self.faces.pop(i)
                    break
                i += 1
        print('[INFO] Remocao de faces realizada')
    
    def find_match(self, desc):
        min_face = None
        min_dist = 1
        for face in self.faces:
            # calcula a distância Euclidiana normalizada (norma de Frobenius) entre dois descritores
            # de face
            dist = numpy.linalg.norm(face['desc'] - desc) 
            if dist < min_dist:
                min_face = face
                min_dist = dist
        
        if min_dist < 0.5:
            self.is_known = True
            self.name = min_face['name']
            return min_face
        else:
            self.is_known = False
            self.name = 'Desconhecido'
            return None
    
    # objeto da caixa delimitadora de face da OpenCV é diferente da Dlib
    def convert_bounds(self, ocv_bounds):
        x, y, w, h = ocv_bounds
        dlib_bounds = dlib.rectangle(left=x, top=y, right=(x+w), bottom=(y+h))
        return dlib_bounds

    def match_face(self, image, ocv_bounds):
        dlib_bounds = self.convert_bounds(ocv_bounds)
        desc = self.gen_face_desc(image, dlib_bounds)
        if (len(desc) > 0):
            return self.find_match(desc)
        else:
            self.is_known = False
            self.name = 'Desconhecido'
            return None


class relay():
    def __init__(self):
        self.pin = 26
        gpio.setmode(gpio.BCM)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)
    
    def switch_on(self):
        gpio.setup(self.pin, gpio.OUT)
        gpio.output(self.pin, gpio.LOW)
    
    def switch_off(self):
        # pino colocado em estado de alta impedância para desligar devido à incompatibilidade das 
        # tensões de operação das GPIO (3.3V) e do módulo relé (5V)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)

# vale a pena testar o sensor separadamente antes de utilizá-lo; nos meus testes, o meu sensor tinha
# medidas irregulares e alcance diminuído quando trabalhava com a Rasp; quando fiz testes com um Arduino,
# funcionou normalmente; não investiguei a fundo esse problema
class ultrasonic():
    def __init__(self):
        gpio.setmode(gpio.BCM)
        # a configuração de um pino perdura mesmo após o fim da execução do programa; com isso, a
        # biblioteca GPIO imprime um warning no início de uma nova execução dizendo que os pinos 
        # já foram configurados; o correto seria que se anulasse essa configuração ao fim do programa
        gpio.setwarnings(False)
        self.pin_trigger = 16
        self.pin_echo = 21
        gpio.setup(self.pin_trigger, gpio.OUT)
        gpio.setup(self.pin_echo, gpio.IN)
        gpio.output(self.pin_trigger, gpio.LOW)
        sleep(1)
    
    def get_distance(self):
        # pulso de trigger
        gpio.output(self.pin_trigger, gpio.HIGH)
        sleep(0.00001)
        gpio.output(self.pin_trigger, gpio.LOW)
 
        t0_start = time()
        t0_stop = time()
 
        while gpio.input(self.pin_echo) == 0:
            t0_start = time()
 
        while gpio.input(self.pin_echo) == 1:
            t0_stop = time()
 
        # a velocidade do som no ar é de 34300 cm/s (lembrando que o sinal vai e volta)
        distance = ((t0_stop - t0_start) * 34300) / 2
        return distance

class api():
    def get_init_faces(self):
        url = 'http://127.0.0.1:5000/get-web-faces'
        msg = requests.get(url)
        obj = msg.json()
        return obj['faces']
    
    # a verificação de atualização de faces foi feita de forma síncrona, por conveniência; penso que 
    # poderia ser aprimorada para funcionar de forma assíncrona
    def update_faces(self, clock, rec):
        t = time()
        # intervalo de tempo arbitrário
        if t - clock > 5:
            url = 'http://127.0.0.1:5000/get-web-update'
            msg = requests.get(url)
            obj = msg.json()
            if obj['to_add']:
                rec.add_faces(obj['add_faces'])
            if obj['to_rem']:
                rec.rem_faces(obj['rem_names'])
            clock = t
        return clock
    
    def get_client(self, client_id):
        param = {}
        param['id'] = client_id
        url = 'http://127.0.0.1:5000/get-client'
        resp = requests.get(url, params=param)
        client_info = resp.json()
        return client_info
    
    def add_log(self, idd, img):
        log = {}
        log['personId'] = idd
        ret, jpg_img = cv2.imencode('.jpg', img)
        b64_img = b64encode(jpg_img)
        log['encodedImage'] = b64_img
        url = 'http://127.0.0.1:5000/add-log'
        requests.post(url, json=log)
        print('[INFO] Registro enviado a Web')
    

class notifier():
    def play_audio(self, audio_bytes):
        # bytes que chegam da plataforma Web ficam no range (-128, +128); porém, o Python requer que
        # eles estejam no range (0, +256)
        for i in range(len(audio_bytes)):
            if audio_bytes[i] < 0:
                audio_bytes[i] = 256 - abs(audio_bytes[i])
        audio_bytes = bytes(audio_bytes)
        # formato de áudio arbitrário; a mudança do convencional mp3 foi uma tentativa de eliminar
        # um efeito que acontece em alguns momentos no fim do áudio, em que a voz sintetizada
        # "dá uma engasgada"; não consegui eliminá-lo
        with open('msg.ogg', 'wb') as file:
            file.write(audio_bytes)
        # talvez tenha uma forma melhor de fazer essa reprodução do que chamar um programa pelo SO
        os.system('omxplayer -local --no-osd msg.ogg')
        os.system('rm msg.ogg')
    
    # um dos membros da minha banca de TCC chamou atenção para o uso do e-mail porque, em caso de
    # um bug, pode haver disparos ininterruptos de e-mails, sobrecarregando o sistema
    def send_email(self, email, msg):
        ctx = ssl.create_default_context()
        with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=ctx) as server:
            server.login('portariaeletronica@gmail.com', 'portariaeesc')
            server.sendmail('portariaeletronica@gmail.com', email, msg)
            
    def send_slack(self, slack_id, msg):
        # não é uma boa prática deixar token exposto no código fonte, mas ele está aqui
        token = 'xoxb-1471991193605-1460346061367-8yZILJXE5W3OXfwB8S9SBDsL'
        client = slack.WebClient(token=token)
        client.chat_postMessage(channel=slack_id, text=msg)
        
    def notify(self, client_name, client_info):
        # claramente essa mensagem pode ser aprimorada
        msg = '[PORTARIA ELETRONICA]\n'
        msg = msg + client_name + ' acessou a portaria e esta a sua espera!'
        self.play_audio(client_info['audioMessage'])
        self.send_email(client_info['sponsorEmail'], msg)
        self.send_slack(client_info['sponsorSlack'], msg)
        
        
        
        
