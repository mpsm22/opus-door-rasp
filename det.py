import cv2
import threading
from time import time, sleep
import picamera
import picamera.array
import queue
import socket
import pickle

NUM_IMG = 100

# despeja os bytes provienentes da captura da câmera em uma stream, coloca-os na fila e trunca a stream
def capture(st):
    # essa função veio da documentação da Pi Camera; a ideia era que se capturasse um determinado
    # número de imagens e se fizesse um processamento em seguida, mas aqui foi adaptada para apenas
    # capturar imagens nessa thread; não sei se seria possível uma implementação com o loop infinito
    # já aqui
    for i in range(NUM_IMG):
        yield st
        queue1.put(st.array)
        st.seek(0)
        st.truncate()
        
def cam_burst():
    while True:
        camera.capture_sequence(capture(stream),format='rgb',use_video_port=True)
        
def socket_server():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((socket.gethostname(), 1753))
    s.listen(5)

    while True:
        clientsocket, address = s.accept()
        # o servidor socket só envia faces recentes, arbitrado como sendo aquelas capturadas há um
        # segundo, no máximo
        while True:
            obj = queue2.get()
            t = time()
            if t - obj['tim'] < 1:
                break
        byt = pickle.dumps(obj)
        clientsocket.send(byt)
        print('[INFO] Face enviada')
        clientsocket.close()

def detect(image):
        # BGR é a codificação de cores da câmera
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # definição dos parâmetros é explicada na monografia de TCC
        faces = cascade_classifier.detectMultiScale(
                                    gray_image,
                                    scaleFactor=1.2,
                                    minNeighbors=4,
                                    minSize=(96, 96))
        
        # caso se tenham múltiplas faces, escolhe-se aquela com maior área; talvez poderia ser feito
        # de uma forma mais inteligente
        if len(faces) > 0:
            max_bounds = (0,0,0,0)
            max_size = 0
            for (x,y,w,h) in faces:
                 if w*h > max_size:
                     max_size = w*h
                     max_bounds = (x,y,w,h)
            return gray_image, max_bounds
        else:
            return gray_image, None
 
# filtro de nitidez é explicado na monografia do TCC; não expus lá, mas acho que não fez tanta diferença
# pra implementação
def is_blurry(image, bounds):
    x,y,w,h = bounds
    img = image[y:y+h, x:x+w]
    var = cv2.Laplacian(img, cv2.CV_64F).var()
    return var < 45

# a face detectada pelo OpenCV possui uma caixa bem estreita, não compreendendo orelhas e queixo, por
# exemplo; sendo w por h as dimensões dessa caixa, esta função recorta a imagem para
# dimensões 3w por 3h
def format_face(img, bnd):
    x,y,w,h = bnd
    tlx = x
    tly = y
    brx = x+w
    bry = y+h
    lmg = min(x,w)
    tmg = min(y,h)
    rmg = min(618-brx, w)
    bmg = min(1392-bry, h)
    
    dic = {}
    dic['img'] = img[tly-tmg : bry+bmg, tlx-lmg : brx+rmg]
    dic['bnd'] = (lmg,tmg,w,h)
    dic['tim'] = time()
    return dic


if __name__ == '__main__':
    cascade_classifier = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')
    queue1 = queue.Queue()
    # tamanho da fila totalmente arbitrário; penso que essa dinâmica de se utilizar uma fila para
    # transferência de faces por socket poderia ser aprimorada
    queue2 = queue.Queue(3)
    
    with picamera.PiCamera() as camera:
            with picamera.array.PiRGBArray(camera) as stream:
                # definição da resolução é explicada na monografia de TCC
                camera.resolution = (1856, 1392)
                camera.framerate = 15
                camera.start_preview()
                sleep(2)
                
                t = threading.Thread(target=cam_burst)
                t.start()
                
                t2 = threading.Thread(target=socket_server)
                t2.start()
                
                print('[INFO] Programa de deteccao inicializado')
                
                while True:
                    print('... capturando e detectando ...')
                    count = 0
                    while count < NUM_IMG:
                        image = queue1.get()
                        # é utilizado apenas o terço central da imagem; pode-se verificar se não seria
                        # necessária uma área maior
                        image = image[:, 619:1237]
                        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                        
                        # linhas que permitem que se visualize o que está sendo capturado 
                        #cv2.imshow('img', cv2.resize(image, (384, 768)))
                        #if cv2.waitKey(1) == 27:
                        #    exit()
                        
                        count += 1
                        gray_img, bounds = detect(image)
                        if bounds is not None:
                            if not is_blurry(gray_img, bounds):
                                face = format_face(image, bounds)
                                if queue2.full():
                                    queue2.get()
                                queue2.put(face)
            








