from flask import Flask, request, jsonify
from door import recognizer
from cv2 import imdecode, IMREAD_COLOR
from numpy import frombuffer, uint8
from json import dumps

app = Flask(__name__)

@app.route('/add-face', methods=['POST'])
def add_face():
    response = request.json
    name = response['name']
    photo = response['photo']
    
    for i in range(len(photo)):
        if (photo[i] < 0):
            photo[i] = 128 - abs(photo[i]) + 128
    img_bytes = bytes(photo)
    img_nparr = frombuffer(img_bytes, uint8)
    img = imdecode(img_nparr, IMREAD_COLOR)
    face_desc = rec.gen_face_descriptor(img)
    
    face = {}
    face['name'] = name
    face['desc'] = face_desc.tolist()
    face_buffer.append(face)
    
    if len(face_desc) > 0:
        return jsonify({'added': True})
    else:
        return jsonify({'added': False})


@app.route('/get-faces', methods=['GET'])
def get_faces():
    is_buffer_empty = len(face_buffer) == 0
    response = {'empty': is_buffer_empty, 'faces' : face_buffer}
    json_res = dumps(response)
    face_buffer.clear()
    return json_res

if __name__ == '__main__':
    rec = recognizer()
    face_buffer = []
    app.run(host='0.0.0.0', debug=True)