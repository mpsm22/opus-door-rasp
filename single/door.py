import cv2
import dlib
import numpy
import RPi.GPIO as gpio
from time import time, sleep
import os
import requests


def fstr(float):
    return '{:.2}'.format(float)

# Classe para deteccao de faces
class detector():
    def __init__(self, folder = 'frames'):
        # Inicializa classe para captura de video utilizando a camera e modo de captura default do SO
        self.cam = cv2.VideoCapture(0)
        # Define resolucao da imagem capturada
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1856)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1392)
        # Define numero de frames armazenados na memoria
        self.cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        # Formata o diretorio onde vai guardar os frames que contem faces
        self.directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), folder)
        # Instancia objeto do classificador 
        self.face_cascade = cv2.CascadeClassifier('../lbpcascade_frontalface_improved.xml')
        self.image = None
        self.bounds = None
        self.tim_detec_process = 0
        self.tim_last_loop = 0
        self.tim_total_search = 0
        self.tim_cam_readings = []
        self.num_input = 0
        
    def search_face(self):
        self.num_input = 0
        t0i = time()
        self.tim_cam_readings.clear()
        while True:
            is_there_frame, image = self.cam.read()
            if is_there_frame:
                self.num_input += 1
                image = image[:, 619:1237]
                gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                # Verifica se ha faces na imagem
                faces = self.face_cascade.detectMultiScale(
                    gray_image,
                    scaleFactor=1.2,
                    minNeighbors=4,
                    minSize=(135, 135))
                
                if len(faces) > 0:
                    print('FACE')
                    # Escolhe a maior imagem
                    max_face = (0,0,0,0)
                    max_face_size = 0
                    for (x,y,w,h) in faces:
                         if w*h > max_face_size:
                             max_face_size = w*h
                             max_face = (x,y,w,h)
                    face_image = gray_image[max_face[1] : (max_face[1] + max_face[3]), max_face[0] : (max_face[0] + max_face[2])]
                    if not self.is_blurry(face_image):        
                        self.image = image
                        self.bounds = max_face
                        break
                else:
                    print('no face')

    def is_blurry(self, image):
        var = cv2.Laplacian(image, cv2.CV_64F).var()
        return var < 45

    def clear_buffer(self):
        self.cam.grab()
    
    def print(self):
        print('DETECTOR TIMING')
        print('    total search: ' + fstr(self.tim_total_search))
        print('    last loop: ' + fstr(self.tim_last_loop))
        print('    detec process: ' + fstr(self.tim_detec_process))
        i = 0
        for tim_cam_read in self.tim_cam_readings:
            print('    cam read' + str(i) + ': ' + fstr(tim_cam_read))
            i += 1
        print()


class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('../recognition.dat')
        self.shape_predictor = dlib.shape_predictor('../shape.dat')
        self.face_detector = dlib.get_frontal_face_detector()
        self.ref_faces = []
        self.ref_names = []
        self.is_known = False
        self.name = ' '
        self.distance = 1000
        self.tim_encode = 0
        self.tim_total = 0
        print('Recognizer initialized')
    
    # Converte uma face em um descritor de 128D
    def encode_face(self, face_image, face_bounds):
        face_landmarks = self.shape_predictor(face_image, face_bounds) # estrutura com pose da face na imagem
        face_chip = dlib.get_face_chip(face_image, face_landmarks) # face alinhada com resolucao 150x150
        t1i = time()
        face_encoding = numpy.array(self.recognition_model.compute_face_descriptor(face_chip, 1)) # numpy array de 128 posicoes
        t1f = time()
        self.tim_encode = t1f - t1i
        return face_encoding

    def gen_face_descriptor(self, face_image):
        faces_bounds = self.face_detector(face_image)
        if len(faces_bounds) > 0:
            face_bounds = faces_bounds[0]
            return self.encode_face(face_image, face_bounds)
        else:
            return None

    def load_ref_faces(self):
        url = 'http://192.168.0.102:8080/get-faces'
        response = requests.get(url)
        faces = response.json()
        for face in faces:
            name = face['name']
            photo = face['photo']
            for i in range(len(photo)):
                if (photo[i] < 0):
                    photo[i] = 128 - abs(photo[i]) + 128
            img_bytes = bytes(photo)
            img_nparr = numpy.frombuffer(img_bytes, numpy.uint8)
            img = cv2.imdecode(img_nparr, cv2.IMREAD_COLOR)
            face_desc = self.gen_face_descriptor(img)
            
            if face_desc is not None:
                self.ref_names.append(name)
                self.ref_faces.append(face_desc)
                print('Face of ' + name + ' was added')
        
    def load_new_faces(self, new_faces):
        for new_face in new_faces:
            self.ref_names.append(new_face['name'])
            self.ref_faces.append(numpy.array(new_face['desc']))

    # Busca uma correspondencia entre o descritor de uma face qualquer e a matriz de descritores conhecidos
    def find_match(self, face_encod):
        matches = numpy.linalg.norm(self.ref_faces - face_encod, axis=1) # calcula a norma de Frobenius para cada array
        min_index = matches.argmin()
        min_value = matches[min_index]
        print(min_value)
        print(self.ref_names[min_index])
        if min_value < 0.5:
            self.is_known = True
            self.name = self.ref_names[min_index]
            self.distance = min_value
        else:
            self.is_known = False
            self.name = 'Unknown'
            self.distance = 1000
    
    # Adiciona margem na imagem para conter toda a cabeca e adequa os limites da face
    def convert_image_input(self, image, ocv_bounds):
        x, y, w, h = ocv_bounds
        crop_top = max(0, y-h)
        crop_bottom = min(1392, y+2*h)
        crop_left = max(0, x-w)
        crop_right = min(618, x+2*w)
        cropped_image = image[crop_top : crop_bottom, crop_left : crop_right]
        dlib_bounds = dlib.rectangle(left=w.item(), top=h.item(), right=(2*w.item()), bottom=(2*h.item()))
        return cropped_image, dlib_bounds

    # Faz o reconhecimento de um face
    def match_face(self, image, ocv_bounds):
        t0i = time()
        cropped_image, dlib_bounds = self.convert_image_input(image, ocv_bounds)
        face_encodings = self.encode_face(cropped_image, dlib_bounds)
        if (len(face_encodings) > 0):
            self.find_match(face_encodings)
        else:
            self.is_known = False
            self.name = 'No Face'
            self.distance = 1000
        t0f = time()
        self.tim_total = t0f - t0i
        
    def print(self):
        print('RECOGNIZER TIMING')
        print('    name: ' + self.name)
        print('    total: ' + fstr(self.tim_total))
        print('    encode: ' + fstr(self.tim_encode))
        print()


class relay():
    def __init__(self):
        self.pin = 26
        gpio.setmode(gpio.BCM)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)
    
    def pulse(self):
        gpio.setup(self.pin, gpio.OUT)
        gpio.output(self.pin, gpio.LOW)
        sleep(0.5)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)

class ultrasonic():
    def __init__(self):
        gpio.setmode(gpio.BCM)
        self.pin_trigger = 16
        self.pin_echo = 21
        gpio.setup(self.pin_trigger, gpio.OUT)
        gpio.setup(self.pin_echo, gpio.IN)
        gpio.output(self.pin_trigger, gpio.LOW)
        sleep(1)
    
    def get_distance(self):
        gpio.output(self.pin_trigger, gpio.HIGH)
        sleep(0.00001)
        gpio.output(self.pin_trigger, gpio.LOW)
 
        t0_start = time()
        t0_stop = time()
 
        while gpio.input(self.pin_echo) == 0:
            t0_start = time()
 
        while gpio.input(self.pin_echo) == 1:
            t0_stop = time()
 
        distance = ((t0_stop - t0_start) * 34300) / 2
        return distance

class collector():
    def get_new_faces(self):
        url = 'http://127.0.0.1:5000/get-faces'
        response = requests.get(self.url)
        json_data = response.json()
        if json_data['empty']:
            return None
        else:
            return json_data['faces']
        
        