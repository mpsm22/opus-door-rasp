import door
from time import time

det = door.detector()
rec = door.recognizer()
rly = door.relay()
son = door.ultrasonic()
col = door.collector()

rec.load_ref_faces()
i = 0
while True:
    print('===============================')
    print()
        
    while(son.get_distance() > 150):
        continue
    
    rec.name = ''
    tot_input = 0
    while rec.name != 'Carolina Dias':
        det.search_face()
        tot_input += det.num_input
        print('RECOGNIZE')
        rec.match_face(det.image, det.bounds)
        
    rly.pulse()
    
    with open('log.txt', 'a') as file:
        file.write(('%d\n') % (det.num_input))
    
    i += 1
    print('----------')
    print(str(i).rjust(3) + ' | ' + str(tot_input))
    print('----------')
    door.sleep(2)
    det.clear_buffer()
    rly.pulse()
