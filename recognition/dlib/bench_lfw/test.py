import dlib
import cv2
import numpy
import os
import pickle

class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('../../recognition.dat')
        self.shape_predictor = dlib.shape_predictor('../../shape.dat')
        self.face_detector = dlib.get_frontal_face_detector()
        self.faces = []

    def load_ref_faces(self, ref_file):
        with open('./' + ref_file, 'rb') as file:
            self.faces = pickle.load(file)

    def find_match(self, threshold, face_encod):
        min_dist1 = 1
        min_name1 = ''
        min_dist2 = 1
        min_name2 = ''
        for face in self.faces:
            dist = numpy.linalg.norm(face['descriptor'] - face_encod)
            if dist < min_dist1:
                min_dist2 = min_dist1
                min_name2 = min_name1
                min_dist1 = dist
                min_name1 = face['name']
            elif dist < min_dist2:
                min_dist2 = dist
                min_name2 = face['name']
                
        return min_name1, min_name2, min_dist1, min_dist2

    def match_face(self, threshold, descriptor):
        if (len(descriptor) > 0):
            return self.find_match(threshold, descriptor)
        else:
            print('[ERROR] no face')
            return None, None, None, None
    
    
if __name__ == '__main__':
    rec = recognizer()
    rec.load_ref_faces('lfw_ref')
    print('RECOGNIZER INITIALIZED')
        
    smp_faces = []
    with open('./lfw_smp', 'rb') as file:
        smp_faces = pickle.load(file)
    
    threshold = 0.5
    for face in smp_faces:
        sname = face['name']
        rname1, rname2, mdist1, mdist2 = rec.match_face(threshold, face['descriptor'])
        if rname1 is None:
            continue
        else:
            if mdist1 > threshold or rname1 != sname:
                #pass
                print('FN: ' + sname + ' | ' + rname1 + ' | ' + str(mdist1))
            if mdist2 < threshold:
                pass
                #print('FP: ' + sname + ' | ' + rname2 + ' | ' + str(mdist2))
























