import dlib
import cv2
import numpy
import os
import pickle

class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('../../recognition.dat')
        self.shape_predictor = dlib.shape_predictor('../../shape.dat')
        self.face_detector = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')
        self.faces = []

    def load_ref_faces(self, ref_file):
        with open('./' + ref_file, 'rb') as file:
            self.faces = pickle.load(file)

    def find_match(self, threshold, face_encod):
        min_dist1 = 1
        min_name1 = ''
        min_dist2 = 1
        min_name2 = ''
        for face in self.faces:
            dist = numpy.linalg.norm(face['descriptor'] - face_encod)
            if dist < min_dist1:
                min_dist2 = min_dist1
                min_name2 = min_name1
                min_dist1 = dist
                min_name1 = face['name']
            elif dist < min_dist2:
                min_dist2 = dist
                min_name2 = face['name']
                
        if min_dist1 > threshold:
            min_name1 = 'Unknown'
        if min_dist2 > threshold:
            min_name2 = 'Unknown'
        return min_name1, min_name2

    def match_face(self, threshold, descriptor):
        if (len(descriptor) > 0):
            return self.find_match(threshold, descriptor)
        else:
            print('[ERROR] no face')
            return None, None
    
    
if __name__ == '__main__':
    rec = recognizer()
    rec.load_ref_faces('lfw_ref')
    print('RECOGNIZER INITIALIZED')
        
    smp_faces = []
    with open('./lfw_smp', 'rb') as file:
        smp_faces = pickle.load(file)
    
    with open('results.txt', 'w') as file:
            file.write('THRESH   TPR   TNR \n')
    
    threshold = 0.45
    while threshold < 0.605:
        print('THRESHOLD ' + '{:.2f}'.format(threshold))
        ntry = 0
        tpos = 0
        tneg = 0
        for face in smp_faces:
            sname = face['name']
            rname1, rname2 = rec.match_face(threshold, face['descriptor'])
            if rname1 is None:
                continue
            else:
                ntry += 1
                if rname1 == sname:
                    tpos += 1
                if rname2 == 'Unknown':
                    tneg += 1
        with open('results.txt', 'a') as file:
            thr_str = '{:.2f}'.format(threshold).center(6)
            TPR_str = '{:.2f}'.format(100*tpos/ntry).rjust(6)
            TNR_str = '{:.2f}'.format(100*tneg/ntry).rjust(6)
            file.write(('%s %s %s\n') % (thr_str, TPR_str, TNR_str))
        threshold += 0.01
























