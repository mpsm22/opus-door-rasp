import dlib
import cv2
import numpy
import os
import pickle

class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('../../recognition.dat')
        self.shape_predictor = dlib.shape_predictor('../../shape.dat')
        self.face_detector = dlib.get_frontal_face_detector()
    
    def encode_face(self, face_img):
        ocv_bounds = self.face_cascade.detectMultiScale(
                        gray_image,
                        scaleFactor=1.1,
                        minNeighbors=5,
                        minSize=(50, 50))
        if len(ocv_bounds) != 1:
            print('[ERROR] there must be only 1 face per image')
            return None
        face_bounds = faces_bounds[0]
        face_landmarks = self.shape_predictor(face_img, face_bounds)
        face_chip = dlib.get_face_chip(face_img, face_landmarks)
        face_encoding = numpy.array(self.recognition_model.compute_face_descriptor(face_chip, 1)) # numpy array de 128 posicoes
        return face_encoding

    def gen_faces_file(self, img_folder, out_file):
        img_files = []
        dir_files = os.listdir(img_folder)
        for file in dir_files:
            if file.endswith('.jpg'):
                img_files.append(file)
        img_files = sorted(img_files)
        
        faces = []
        for img_file in img_files:
            face = {}
            face['name'] = img_file[:-9]
            img_path = img_folder + '/' + img_file
            face_img = cv2.imread(img_path)
            descriptor = self.encode_face(face_img)
            if descriptor is None:
                continue
            else:
                face['descriptor'] = descriptor
            gray_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
            face['sharpness'] = int(cv2.Laplacian(gray_img, cv2.CV_64F).var())
            face['img_dim'] = face_img.shape
            faces.append(face)
            print('    ' + face['name'] + ' loaded')
        with open('./' + out_file, 'wb') as file:
            pickle.dump(faces, file)
    
    
if __name__ == '__main__':
    rec = recognizer()
    print('LOADING REFERENCE...')
    rec.gen_faces_file('./lfw/reference', 'lfw_ref')
    print('LOADING SAMPLES...')
    rec.gen_faces_file('./lfw/samples', 'lfw_smp')






















