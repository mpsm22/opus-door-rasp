import statistics
import matplotlib.pyplot as plt

with open('timing2.txt', 'r') as file:
    line = file.readline()
    time_list = []
    while len(line) > 0:
        tok = line.split()
        time_list.append(float(tok[2]))
        line = file.readline()

print('mean: ' + str(statistics.mean(time_list)))
print('stdev: ' + str(statistics.stdev(time_list)))
gt1500ms = len(list(filter(lambda t: t > 1.5, time_list)))/len(time_list)
gt2000ms = len(list(filter(lambda t: t > 2.0, time_list)))/len(time_list)
print('% > 1.5s: ' + str(gt1500ms))
print('% > 2.0s: ' + str(gt2000ms))
plt.hist(time_list, bins='auto')
plt.show()