import door
import os
import time
    
aws_key = os.environ['AWS_KEY']
aws_secret = os.environ['AWS_SECRET']
collect = 'Door'

det = door.detect()
rek = door.rekog(aws_key, aws_secret, collect)
per = door.person()

for i in range(100):
    print(i)
    det.search_face()
    rek.id_face(det.image_path, per)
    with open('log.txt', 'a') as file:
        sharp_str = '{:.0f}'.format(det.sharpness)
        time_str = '{:.1f}'.format(per.get_rek_time())
        file.write(('%s %s %s\n') % (per.get_label(), sharp_str, time_str))
