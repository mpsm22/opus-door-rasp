import boto3
import numpy as np
import cv2
import time
import os


class detect():
    def __init__(self, folder = 'frames'):
        self.cam = cv2.VideoCapture(0)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1088)
        self.cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        self.face_cascade = cv2.CascadeClassifier('../lbpcascade_frontalface_improved.xml')
        self.directory = folder
        self.image_path = ''
        self.sharpness = 0
        
    def search_face(self):  
        while True:
            is_there_frame, image = self.cam.read() 
            if is_there_frame:
                image = image[0:1088, 640:1280]
                gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                faces = self.face_cascade.detectMultiScale(
                    gray_image,
                    scaleFactor=1.1,
                    minNeighbors=3,
                    minSize=(50,50))
                
                if len(faces) > 0:                   
                        for (x,y,w,h) in faces:
                            top_margin = 0.9
                            side_margin = 1.3
                            bottom_margin = 1.4
                            new_top_left_x = max(0, int(x - side_margin*w))
                            new_top_left_y = max(0, int(y - top_margin*h))
                            new_top_right_x = min(1920, int(x + (1+side_margin)*w))
                            new_bottom_left_y = min(1088, int(y + (1+bottom_margin)*h))
                            image_crop = image[new_top_left_y : new_bottom_left_y,
                                new_top_left_x : new_top_right_x]
                            self.meas_sharpness(image_crop)
                            time_stamp = time.strftime("%d-%m-%Y_%H:%M:%S")
                            self.image_path = '{0}/frame_{1}.jpg'.format(self.directory, time_stamp)
                            cv2.imwrite(self.image_path, image_crop)
                            break
                        break
    
    def meas_sharpness(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        self.sharpness = cv2.Laplacian(gray, cv2.CV_64F).var()


class rekog():
    def __init__(self, aws_key, aws_sec_key, aws_collec):
        self.__client = boto3.client('rekognition',
                        aws_access_key_id = aws_key,
                        aws_secret_access_key = aws_sec_key,
                        region_name = 'us-west-2')
        self.__collection = aws_collec

    def get_client(self):
        return self.__client

    def id_face(self, photo_file, pers):
        start_time = time.time()
        if os.path.exists(photo_file):
            with open(photo_file, 'rb') as image:
                try:
                    response = (self.__client).search_faces_by_image(CollectionId=self.__collection,
                                    Image={'Bytes': image.read()},
                                    MaxFaces=1)
                except self.__client.exceptions.InvalidParameterException:
                    response = None
            stop_time = time.time()     
            rek_time = stop_time - start_time
            
            if response == None:
                pers.set('No_Faces', 0.0, 0.0, rek_time)
            
            elif (response['FaceMatches']):
                label = response['FaceMatches'][0]['Face']['ExternalImageId']
                similarity = response['FaceMatches'][0]['Similarity']
                confidence = response['FaceMatches'][0]['Face']['Confidence']
                pers.set(label, similarity, confidence, rek_time)
            else:
                pers.set('Unknown', 0.0, 0.0, rek_time)
        else:
            stop_time = time.time()     
            rek_time = stop_time - start_time
            pers.set('PhotoNotFound', 0.0, 0.0, rek_time)


class person():
    def __init__(self):
        self.__label = 'Unknown'
        self.__similarity = 0.0
        self.__confidence = 0.0
        self.__rek_time = 0.0

    def set(self, lab, simil, conf, rtim):
        self.__label = lab
        self.__similarity = simil
        self.__confidence = conf
        self.__rek_time = rtim
    
    def get_label(self):
        return self.__label

    def get_similarity(self):
        return self.__similarity
    
    def get_confidence(self):
        return self.__confidence

    def get_rek_time(self):
        return self.__rek_time
