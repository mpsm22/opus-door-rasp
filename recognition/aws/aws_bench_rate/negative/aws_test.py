import aws
import os, os.path
from time import localtime, strftime

aws_key = os.environ['AWS_KEY']
aws_secret = os.environ['AWS_SECRET']
collection = 'lfw200'


rek = aws.rekog(aws_key, aws_secret, collection)
per = aws.person()
        
faces = rek.list_faces()
for face in faces:   
    rek.remove_face(face['id'])
    for i in range(2, 5+1):
        file_path = '../lfw/' + face['name'] + '/' + face['name'] + '_000' + str(i) + '.jpg'
        if os.path.exists(file_path):
            rek.id_face(file_path, per)
            with open('results.txt', 'a') as file:
                sim_str = '{:.2f}'.format(per.get_similarity())
                rname = per.get_label()
                file.write(('%s %s %s\n') % (face['name'], rname, sim_str))
        else:
            break
    rek.add_face(face['name'])
                
                
                
                