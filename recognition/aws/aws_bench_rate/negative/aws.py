import boto3
import numpy as np
import cv2
import time
import os

class rekog():
    def __init__(self, aws_key, aws_secret, aws_collection):
        self.client = boto3.client('rekognition',
                        aws_access_key_id = aws_key,
                        aws_secret_access_key = aws_secret,
                        region_name = 'us-west-2')
        self.collection = aws_collection
    
    def list_faces(self):
        response = self.client.list_faces(CollectionId=self.collection)
        faces = []
        for aws_face in response['Faces']:
            face = {}
            face['id'] = aws_face['FaceId']
            face['name'] = aws_face['ExternalImageId']
            faces.append(face)
        return faces
    
    def add_face(self, name):
        with open('../lfw/' + name + '/' + name + '_0001.jpg', 'rb') as image:
            self.client.index_faces(Image={'Bytes': image.read()},
                        CollectionId=self.collection,
                        ExternalImageId=name,
                        MaxFaces=1,
                        DetectionAttributes=['ALL'])
    
    def remove_face(self, face_id):
        face_id_list = [face_id]
        self.client.delete_faces(CollectionId=self.collection, FaceIds=face_id_list)

    def id_face(self, photo_file, pers):
        start_time = time.time()
        if os.path.exists(photo_file):
            with open(photo_file, 'rb') as image:
                try:
                    response = (self.client).search_faces_by_image(CollectionId=self.collection,
                                    Image={'Bytes': image.read()},
                                    MaxFaces=1,
                                    FaceMatchThreshold=0)
                except self.__client.exceptions.InvalidParameterException:
                    response = None
            stop_time = time.time()     
            rek_time = stop_time - start_time
            
            if response == None:
                pers.set('No_Faces', 0.0, 0.0, rek_time)
            
            elif (response['FaceMatches']):
                label = response['FaceMatches'][0]['Face']['ExternalImageId']
                similarity = response['FaceMatches'][0]['Similarity']
                confidence = response['FaceMatches'][0]['Face']['Confidence']
                pers.set(label, similarity, confidence, rek_time)
            else:
                pers.set('Unknown', 0.0, 0.0, rek_time)
        else:
            stop_time = time.time()     
            rek_time = stop_time - start_time
            pers.set('PhotoNotFound', 0.0, 0.0, rek_time)


class person():
    def __init__(self):
        self.__label = 'Unknown'
        self.__similarity = 0.0
        self.__confidence = 0.0
        self.__rek_time = 0.0

    def set(self, lab, simil, conf, rtim):
        self.__label = lab
        self.__similarity = simil
        self.__confidence = conf
        self.__rek_time = rtim
    
    def get_label(self):
        return self.__label

    def get_similarity(self):
        return self.__similarity
    
    def get_confidence(self):
        return self.__confidence

    def get_rek_time(self):
        return self.__rek_time
