for threshold in range(0,95+1):
	f = open('aws_results.txt', 'r')
	line = f.readline()
	ntry = 0
	nsucess = 0
	while (len(line) > 0):
		tokens = line.split()
		input = tokens[0]
		output = tokens[1]
		confidence = float(tokens[2])
		input = input.split('0')
		input = input[0]
		ntry = ntry + 1
		if (confidence > threshold and input == (output + '_')):
			nsucess = nsucess + 1
		line = f.readline()
	f.close()
	with open('aws_accuracy.txt', 'a') as file:
		accuracy = str("{:.2f}".format(100*nsucess/ntry))
		file.write(('%s %s\n') % (str(threshold), accuracy))
