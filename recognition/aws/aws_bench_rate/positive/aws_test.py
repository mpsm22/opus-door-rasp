import aws
import os, os.path
from time import localtime, strftime

aws_key = os.environ['AWS_KEY']
aws_secret = os.environ['AWS_SECRET']
collection = 'lfw200'


#Instantiate main objects
rek = aws.rekog(aws_key, aws_secret, collection)
per = aws.person()

#Create list of names
f = open('lfw_names.txt', 'r')
list = []
line = f.readline()
while (len(line) > 0):
        tokens = line.split()
        list.append(tokens[0])
        line = f.readline()
f.close()

#Try to recognize the faces not in the collection
count = 0
for name in list:
    for i in range(2, 5+1):
        file_path = './lfw/' + name + '/' + name + '_' + str(i).zfill(4) + '.jpg'
        if (os.path.exists(file_path)):
            rek.id_face(file_path, per)
            count = count + 1
            print(count)
            with open('aws_timing.txt', 'a') as file:
                rek_time = str(per.get_rek_time())
                file.write(('%s\n') % (rek_time))
            with open('aws_results.txt', 'a') as file:
                input = name + '_' + str(i).zfill(4)
                output = per.get_label()
                confidence = str(per.get_similarity())
                file.write(('%s   %s   %s\n') % (input, output, confidence))

