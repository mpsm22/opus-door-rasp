import collection

#Create list of names
f = open('lfw_names.txt', 'r')
list = []
line = f.readline()
while (len(line) > 0):
	tokens = line.split()
	list.append(tokens[0])
	line = f.readline()
f.close()

#Add to collection
for name in list:
	collection.add_face(name, './lfw/' + name + '/' + name + '_0001.jpg', 'lfw200')

