# -*- coding: utf-8 -*-

import door
import os

aws_key = os.environ['AWS_KEY']
aws_secret = os.environ['AWS_SECRET']


def create_collection(label):
	rek_client = (door.rekog(aws_key, aws_secret, collection)).get_client()
	response = rek_client.create_collection(CollectionId=label)
	print('Collection ARN: ' + response['CollectionArn'])
	print('Status code: ' + str(response['StatusCode']))
	print('Done')

def add_face(label, file, collection):
	rek_client = (door.rekog(aws_key, aws_secret, collection)).get_client()
	with open(file, 'rb') as image:
		response = rek_client.index_faces(Image={'Bytes': image.read()},
						CollectionId=collection,
						ExternalImageId=label,
						DetectionAttributes=['ALL'])
	print(label + ' added')
                          
