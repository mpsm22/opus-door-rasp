import random

#Create list of names with 2 or more images
f = open('lfw-names.txt', 'r')
list = []
line = f.readline()
while (len(line) > 0):
	tokens = line.split()
	if (int(tokens[1]) > 1):
		list.append(tokens[0])
	line = f.readline()
f.close()

number_list = random.sample(range(0, len(list)-1), 100)
random_list = []
for i in number_list:
	random_list.append(list[i])

for name in random_list:
	print(name)
