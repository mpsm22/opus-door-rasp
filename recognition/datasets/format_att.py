import os
from cv2 import imread, imwrite

folders = os.listdir('./subset_of_orig')
for folder in folders:
    files = os.listdir('./subset_of_orig/' + folder)
    i = 1
    dest_folder = './att/reference'
    for file in files:
        img = imread('./subset_of_orig/' + folder + '/' + file)
        img_new_path = dest_folder + '/' + folder.upper() + '_000' + str(i) + '.jpg'
        imwrite(img_new_path, img)
        i += 1
        if i == 2:
            dest_folder = './att/samples'