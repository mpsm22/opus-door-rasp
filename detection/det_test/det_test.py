import cv2
import time
import os


cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1856)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1392)
cam.set(cv2.CAP_PROP_BUFFERSIZE, 2)
folder = 'frames'
directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), folder)
face_cascade = cv2.CascadeClassifier('../../lbpcascade_frontalface_improved.xml')
image_path = ' '
    
sum_time_cam = 0
sum_time_cvt = 0
while True:
#for i in range(100):
    t0_start = time.time()
    # Pega, decodifica e retorna proximo frame do video
    is_there_frame, image = cam.read()
    t2_flag = time.time()
    sum_time_cam += t2_flag - t0_start
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    t3_flag = time.time()
    sum_time_cvt += t3_flag - t2_flag
    if is_there_frame:
        image = image[0:1392, 619:1237]
        image = cv2.resize(image, (341, 768))
        cv2.imshow('img', image)
        if cv2.waitKey(1) == 27:
            exit()
#         gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#         # Verifica se ha faces na imagem
#         t1_start = time.time()
#         faces = face_cascade.detectMultiScale(
#             gray_image,
#             scaleFactor=1.1,
#             minNeighbors=3,
#             minSize=(130, 130))
#         t1_stop = time.time()
#         #TODO escolher a maior face
#         if len(faces) > 0:
#              for (x,y,w,h) in faces:
#                  t0_stop = time.time()
#                  total_time = t0_stop - t0_start
#                  bounds = (x,y,w,h)
#                  break
#              break

print('cam time: ' + str(sum_time_cam/100))
print('cvt time: ' + str(sum_time_cvt/100))
