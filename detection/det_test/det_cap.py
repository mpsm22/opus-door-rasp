import cv2
import time
import os


cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1856)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1392)
cam.set(cv2.CAP_PROP_BUFFERSIZE, 2)
folder = 'frames'
directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), folder)
face_cascade = cv2.CascadeClassifier('../../lbpcascade_frontalface_improved.xml')
time.sleep(3)
for j in range(1):
    img_list = []
    bnd_list = []

    i = 0
    while i < 5:
        time.sleep(0.5)
        is_there_frame, image = cam.read()
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if is_there_frame:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # Verifica se ha faces na imagem
            faces = face_cascade.detectMultiScale(
                gray_image,
                scaleFactor=1.1,
                minNeighbors=4,
                minSize=(50, 50))
            if len(faces) > 0:
                i += 1
                print(i)
                img_list.append(image)
                for (x,y,w,h) in faces:
                    bnd_list.append((x,y,w,h))
                    break

    print('SAVING ' + str(j))
    with open('faces.txt', 'a') as file:
        k = 0
        for (x,y,w,h) in bnd_list:
            img = img_list[k]
            path = './frames/face' + str(20*(j+5)+k) + '.jpg'
            cv2.imwrite(path, img)
            file.write(('%d %d %d %d %s\n') % (x,y,w,h,path))
            k += 1
        
    
    
    
    
    
    
    
