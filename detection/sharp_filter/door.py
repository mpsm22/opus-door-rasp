import cv2
import dlib
import numpy
import RPi.GPIO as gpio
from time import time, sleep
import os


def fstr(float):
    return '{:.2}'.format(float)

# Classe para deteccao de faces
class detector():
    def __init__(self, folder = 'frames'):
        # Inicializa classe para captura de video utilizando a camera e modo de captura default do SO
        self.cam = cv2.VideoCapture(0)
        # Define resolucao da imagem capturada
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 2592)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1944)
        # Define numero de frames armazenados na memoria
        self.cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        # Formata o diretorio onde vai guardar os frames que contem faces
        self.directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), folder)
        # Instancia objeto do classificador 
        self.face_cascade = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')
        self.image = None
        self.bounds = None
        self.tim_detec_process = 0
        self.tim_last_loop = 0
        self.tim_total_search = 0
        self.tim_cam_readings = []
        
    def search_face(self):
        t0i = time()
        self.tim_cam_readings.clear()
        while True:
            t1i = time()
            # Pega, decodifica e retorna proximo frame do video
            t3i = time()
            is_there_frame, image = self.cam.read()
            t3f = time()
            self.tim_cam_readings.append(t3f - t3i)
            if is_there_frame:
                image = image[0:1944, 864:1728]
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                if True:
                #if not self.is_blurry(image):
                    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    # Verifica se ha faces na imagem
                    t2i = time()
                    faces = self.face_cascade.detectMultiScale(
                        gray_image,
                        scaleFactor=1.1,
                        minNeighbors=3,
                        minSize=(130, 130))
                    t2f = time()
                    self.tim_detec_process = t2f - t2i
                    
                    if len(faces) > 0:
                        # Escolhe a maior imagem
                        bigger_face = (0,0,0,0)
                        bigger_face_size = 0
                        for (x,y,w,h) in faces:
                             if w*h > bigger_face_size:
                                 bigger_face_size = w*h
                                 bigger_face = (x,y,w,h)
                        
                        self.image = image
                        self.bounds = bigger_face
                        t0f = time()
                        t1f = time()
                        self.tim_total_search = t0f - t0i
                        self.tim_last_loop = t1f - t1i
                        break

    def is_blurry(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        var = cv2.Laplacian(gray, cv2.CV_64F).var()
        return var < 50

    def clear_buffer(self):
        self.cam.grab()
    
    def print(self):
        print('DETECTOR TIMING')
        print('    total search: ' + fstr(self.tim_total_search))
        print('    last loop: ' + fstr(self.tim_last_loop))
        print('    detec process: ' + fstr(self.tim_detec_process))
        i = 0
        for tim_cam_read in self.tim_cam_readings:
            print('    cam read' + str(i) + ': ' + fstr(tim_cam_read))
            i += 1
        print()


class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('../recognition.dat')
        self.shape_predictor = dlib.shape_predictor('../shape.dat')
        self.face_detector = dlib.get_frontal_face_detector()
        self.ref_faces = None
        self.ref_names = None
        self.is_known = False
        self.name = ' '
        self.distance = 1000
        self.tim_encode = 0
        self.tim_total = 0
    
    # Converte uma face em um descritor de 128D
    def encode_face(self, face_image, face_bounds):
        face_landmarks = self.shape_predictor(face_image, face_bounds) # estrutura com pose da face na imagem
        face_chip = dlib.get_face_chip(face_image, face_landmarks) # face alinhada com resolucao 150x150
        t1i = time()
        face_encoding = numpy.array(self.recognition_model.compute_face_descriptor(face_chip, 1)) # numpy array de 128 posicoes
        t1f = time()
        self.tim_encode = t1f - t1i
        return face_encoding

    # Carrega uma matriz de descritores a partir de um diretorio de imagens contendo as faces conhecidas
    # do sistema
    def load_ref_faces(self, ref_faces_folder):
        image_files = []
        dir_files = os.listdir(ref_faces_folder)
        for file in dir_files:
            if file.endswith('.jpg'):
                image_files.append(file)
        image_files = sorted(image_files)
        
        person_names = []
        image_paths = []
        for image_file in image_files:
            person_names.append(image_file[:-4])
            image_paths.append(ref_faces_folder + image_file)

        known_faces = []
        for image_path in image_paths:
            face_image = cv2.imread(image_path) # matriz com os valores dos pixels
            
            faces_bounds = self.face_detector(face_image, 0)
            if len(faces_bounds) != 1:
                print('Expected one and only one face per image: ' + image_path)
                print('It has ' + str(len(faces_bounds)))
                exit()
            face_bounds = faces_bounds[0] # formato ((x,y), (w,h))
            
            known_faces.append(self.encode_face(face_image, face_bounds))
            print(image_path)
        self.ref_faces = known_faces
        self.ref_names = person_names

    # Busca uma correspondencia entre o descritor de uma face qualquer e a matriz de descritores conhecidos
    def find_match(self, face_encod):
        matches = numpy.linalg.norm(self.ref_faces - face_encod, axis=1) # calcula a norma de Frobenius para cada array
        min_index = matches.argmin()
        min_value = matches[min_index]
        if min_value < 0.5:
            self.is_known = True
            self.name = self.ref_names[min_index]
            self.distance = min_value
        else:
            self.is_known = False
            self.name = 'Unknown'
            self.distance = 1000
    
    # Adiciona margem na imagem para conter toda a cabeca e adequa os limites da face
    def convert_image_input(self, image, ocv_bounds):
        x, y, w, h = ocv_bounds
        crop_top = max(0, y-h)
        crop_bottom = min(1944, y+2*h)
        crop_left = max(0, x-w)
        crop_right = min(864, x+2*w)
        cropped_image = image[crop_top : crop_bottom, crop_left : crop_right]
        dlib_bounds = dlib.rectangle(left=w, top=h, right=(2*w), bottom=(2*h))
        return cropped_image, dlib_bounds

    # Faz o reconhecimento de um face
    def match_face(self, image, ocv_bounds):
        t0i = time()
        cropped_image, dlib_bounds = self.convert_image_input(image, ocv_bounds)
        start_enc = time()
        face_encodings = self.encode_face(cropped_image, dlib_bounds)
        stop_enc = time()
        if (len(face_encodings) > 0):
            self.find_match(face_encodings)
        else:
            self.is_known = False
            self.name = 'No Face'
            self.distance = 1000
        t0f = time()
        self.tim_total = t0f - t0i
        
    def print(self):
        print('RECOGNIZER TIMING')
        print('    total: ' + fstr(self.tim_total))
        print('    encode: ' + fstr(self.tim_encode))
        print()


class relay():
    def __init__(self):
        self.pin = 21
        gpio.setmode(gpio.BCM)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)
    
    def pulse(self):
        gpio.setup(self.pin, gpio.OUT)
        gpio.output(self.pin, gpio.LOW)
        sleep(1)
        gpio.setup(self.pin, gpio.IN, pull_up_down=gpio.PUD_OFF)

class ultrasonic():
    def __init__(self):
        gpio.setmode(gpio.BCM)
        self.pin_trigger = 26
        self.pin_echo = 16
        gpio.setup(self.pin_trigger, gpio.OUT)
        gpio.setup(self.pin_echo, gpio.IN)
        gpio.output(self.pin_trigger, gpio.LOW)
        sleep(1)
    
    def get_distance(self):
        gpio.output(self.pin_trigger, gpio.HIGH)
        sleep(0.00001)
        gpio.output(self.pin_trigger, gpio.LOW)
 
        t0_start = time()
        t0_stop = time()
 
        while gpio.input(self.pin_echo) == 0:
            t0_start = time()
 
        while gpio.input(self.pin_echo) == 1:
            t0_stop = time()
 
        distance = ((t0_stop - t0_start) * 34300) / 2
        return distance
        
        
        
        
        