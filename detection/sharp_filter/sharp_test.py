import door
import cv2

def get_face_sharpness(img, bnd):
    (x,y,w,h) = bnd
    face = img[y:y+h, x:x+w]
    gray_face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
    var = cv2.Laplacian(gray_face, cv2.CV_64F).var()
    return var


rec = door.recognizer()
rec.load_ref_faces('./reference/')

curr_name = 'Carolina_Dias_0001'
with open('faces.txt', 'r') as file_in:
    i = 0
    line = file_in.readline()
    while len(line) > 0:
        tok = line.split()
        bnd = (int(tok[0]), int(tok[1]), int(tok[2]), int(tok[3]))
        path = tok[4]
        print(path)
        img = cv2.imread(tok[4])
        sharpness = int(get_face_sharpness(img, bnd))
        rec.match_face(img, bnd)
        print(rec.name)
        if rec.name == curr_name:
            match = 1
        else:
            match = 0
        with open('results_pos.txt', 'a') as file_out:
            file_out.write(('%d %d\n') % (sharpness, match))
        line = file_in.readline()
        i += 1
        if i > 99:
            curr_name = 'Marcos_Monteiro_0001'
        
        
carol_index = rec.ref_names.index('Carolina_Dias_0001')
rec.ref_names.pop(carol_index)
rec.ref_faces.pop(carol_index)
marcos_index = rec.ref_names.index('Marcos_Monteiro_0001')
rec.ref_names.pop(marcos_index)
rec.ref_faces.pop(marcos_index)


with open('faces.txt', 'r') as file_in:
    line = file_in.readline()
    while len(line) > 0:
        tok = line.split()
        bnd = (int(tok[0]), int(tok[1]), int(tok[2]), int(tok[3]))
        path = tok[4]
        print(path)
        img = cv2.imread(tok[4])
        sharpness = int(get_face_sharpness(img, bnd))
        rec.match_face(img, bnd)
        print(rec.name)
        if rec.name == 'Unknown':
            match = 1
        else:
            match = 0
        with open('results_neg.txt', 'a') as file_out:
            file_out.write(('%d %d\n') % (sharpness, match))
        line = file_in.readline()
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        