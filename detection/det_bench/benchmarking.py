import numpy as np
import cv2
import time
import os


def detect_faces(file_path, classifier, scale_factor, min_neigh):
    image = cv2.imread(file_path)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = classifier.detectMultiScale(
            gray_image,
            scaleFactor=scale_factor,
            minNeighbors=min_neigh,
            minSize=(45,45))
    scale_factor_str = '{:.0f}'.format(100*scale_factor)
    with open('detections/detection_' + scale_factor_str + '_' + str(min_neigh) + '.txt', 'a') as file:
        file.write(('%s %d ') % (file_path, len(faces)))
        for (x,y,w,h) in faces:
            file.write(('%d %d %d %d ') % (x,y,w,h))
        file.write('\n')


classifier = cv2.CascadeClassifier('../../lbpcascade_frontalface_improved.xml')
scale_factor_list = [1.02, 1.04, 1.06, 1.08, 1.10, 1.12, 1.14, 1.16, 1.18, 1.20]
min_neigh_list = [1, 2, 3, 4, 5]
for scale_factor in scale_factor_list:
    for min_neigh in min_neigh_list:
        i = 0
        f = open('annotations.txt', 'r')
        line = f.readline()
        while (len(line) > 0):
            tokens = line.split()
            file_path = tokens[0]
            detect_faces(file_path, classifier, scale_factor, min_neigh)
            line = f.readline()
            i += 1
            print(str(i) + '/2565 | ' + str(scale_factor) + ' | ' + str(min_neigh))
