f1 = open('raw_annotations.txt', 'r')
line = f1.readline()
number_list = []
face_count = 1
file_path = ' '
while (len(line) > 0):
    tokens = line.split('mtcnn')
    tokens = tokens[1].split('\n')
    if (file_path == '.' + tokens[0]):
        face_count += 1
    else:
        if (file_path != ' '):
            with open('annotations.txt', 'a') as f2:
                f2.write(('%s %d ') % (file_path, face_count))
                for i in range(face_count):
                    f2.write(('%d %d %d %d ') % (number_list[4*i+0], number_list[4*i+1], number_list[4*i+2], number_list[4*i+3]))
                f2.write('\n')
        file_path = '.' + tokens[0]
        face_count = 1
        number_list = []
    line = f1.readline()
    tokens = line.split()
    for i in range(4):
        number_list.append(int(round(float(tokens[i]))))
    line = f1.readline()
