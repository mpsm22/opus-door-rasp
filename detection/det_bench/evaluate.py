def gen_dict_list(line):
    tokens = line.split()
    num_bound = int(tokens[1])
    dict_list = []
    if (num_bound > 0):
        for i in range(num_bound):
            bound_dict = {}
            bound_dict['x1'] = int(tokens[i*4+2])
            bound_dict['y1'] = int(tokens[i*4+3])
            bound_dict['x2'] = bound_dict['x1'] + int(tokens[i*4+4])
            bound_dict['y2'] = bound_dict['y1'] + int(tokens[i*4+5])
            bound_dict['marked'] = False
            dict_list.append(bound_dict)
    return dict_list


def evaluate(annotation_dict_list, detection_dict_list):
    true_positives = 0
    false_negatives = 0
    false_positives = 0
    for annotation in annotation_dict_list:
        max_match_degree = 0
        max_match_detection = None
        for detection in detection_dict_list:
            if (detection['marked'] == False):
                match_degree = calc_match(annotation, detection)
                if (match_degree > max_match_degree):
                    max_match_degree = match_degree
                    max_match_detection = detection
        if (max_match_degree > 0.5):
            true_positives += 1
            max_match_detection['marked'] = True
        else:
            false_negatives += 1
    for detection in detection_dict_list:
        if (detection['marked'] == False):
            false_positives += 1
    return true_positives, false_negatives, false_positives


def calc_match(annotation, detection):
    x_left = max(annotation['x1'], detection['x1'])
    y_top = max(annotation['y1'], detection['y1'])
    x_right = min(annotation['x2'], detection['x2'])
    y_bottom = min(annotation['y2'], detection['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    annotation_area = (annotation['x2'] - annotation['x1']) * (annotation['y2'] - annotation['y1'])
    detection_area = (detection['x2'] - detection['x1']) * (detection['y2'] - detection['y1'])

    if (intersection_area / detection_area > 0.9):
         return 1.0

    match_degree = intersection_area / float(annotation_area + detection_area - intersection_area)
    return match_degree



with open('results.txt', 'w') as file:
    file.write('SCALE_FACTOR MIN_NEIGH TPR% FPR%\n') 
min_neigh_list = [1, 2, 3, 4, 5]
scale_factor_list = [102, 104, 106, 108, 110, 112, 114, 116, 118, 120]
for min_neigh in min_neigh_list:
    for scale_factor in scale_factor_list:
        file_ann = open('annotations.txt', 'r')
        file_det = open('detections/detection_' + str(scale_factor) + '_' + str(min_neigh) + '.txt', 'r')
        ann_line = file_ann.readline()
        det_line = file_det.readline()
        sum_tp = 0
        sum_fn = 0
        sum_fp = 0
        while (len(ann_line) > 0 or len(det_line) > 0):
            annotation_dict_list = gen_dict_list(ann_line)
            detection_dict_list = gen_dict_list(det_line)
            tp, fn, fp = evaluate(annotation_dict_list, detection_dict_list)
            sum_tp += tp
            sum_fn += fn
            sum_fp += fp
            ann_line = file_ann.readline()
            det_line = file_det.readline()
        scale_factor_s = '{:.1f}'.format(scale_factor).center(12)
        min_neigh_s = str(min_neigh).center(9)
        tpr_s = '{:.1f}'.format(100*sum_tp/(sum_tp + sum_fn)).rjust(4)
        fpr_s = '{:.1f}'.format(100*sum_fp/(sum_tp + sum_fp)).rjust(4)
        with open('results.txt', 'a') as file:
            file.write(('%s %s %s %s\n') % (scale_factor_s, min_neigh_s, tpr_s, fpr_s))
