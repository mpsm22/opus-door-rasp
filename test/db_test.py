import mysql.connector
import cv2
import time
import io
import numpy as np

db = mysql.connector.connect(host='192.168.0.102', user='door_db_admin', passwd='my20TCC!', database='door_db')
db_cursor = db.cursor()
db_cursor.execute('SELECT photo FROM door_db.person;')
result = db_cursor.fetchone()
bytes_img = result[0]
nparr = np.frombuffer(bytes_img, np.uint8)
print(type(nparr))
img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
cv2.imshow('img', img_np)
if cv2.waitKey(100) == 27:
    print('quit')
