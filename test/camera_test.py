# import the necessary packages
from __future__ import print_function
from pi_camera import PiVideoStream
from picamera.array import PiRGBArray
from picamera import PiCamera
import argparse
import time
import cv2

# created a *threaded *video stream, allow the camera sensor to warmup,
# and start the FPS counter
print("[INFO] sampling THREADED frames from `picamera` module...")
vs = PiVideoStream(resolution=(1856,1392), framerate=20).start()
time.sleep(2.0)
ti = time.time()
# loop over some frames...this time using the threaded stream
counter0 = -1
num_frames = 0
while num_frames < 30:
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    frame, counter = vs.read()
    if counter != counter0:
        print(counter)
        counter0 = counter
        num_frames += 1
# stop the timer and display FPS information
tf = time.time()
print(30/(tf-ti))
print(counter0)
vs.stop()
