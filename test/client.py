import socket
import io
import cv2
import pickle
from time import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((socket.gethostname(), 1234))

stream = io.BytesIO()
ti = time()
while True:
    msg = s.recv(1024)
    if len(msg) <= 0:
        break
    stream.write(msg)

dic = pickle.loads(stream.getvalue())
tf = time()
print(dic['nam'])
print(tf-ti)
cv2.imshow('img', dic['img'])
cv2.waitKey(1000)
