import numpy as np
import io
import time
import picamera
import queue
import cv2
import picamera.array


Numberofimages=50


def outputs(stream):
    ti = time.time()
    for i in range(Numberofimages):
        yield stream
        
        #print(stream.getbuffer().nbytes)
        img_queue.put(stream.array)
        #print(stream.getbuffer().nbytes)
        #print(type(value))
        #print(len(value))
        
        stream.seek(0)
        #print(stream.getbuffer().nbytes)
        
        stream.truncate()
        #print(stream.getbuffer().nbytes)
        #print()


with picamera.PiCamera() as camera:
    with picamera.array.PiRGBArray(camera) as stream:
        img_queue = queue.Queue()
        
        camera.resolution = (2230, 1672)
        camera.framerate = 30
        camera.start_preview()
        time.sleep(2)
        
        start = time.time()
        camera.capture_sequence(outputs(stream),format='rgb',use_video_port=True)
        finish = time.time()
        print('Captured images at %.2ffps' % (Numberofimages / (finish - start)))
     
    img = img_queue.get()
    print(img.shape)
