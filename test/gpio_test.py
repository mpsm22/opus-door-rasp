import RPi.GPIO as GPIO
import time


def pin_out_float(pin):
    GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_OFF)

def pin_out_low(pin):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)

def pin_out_high(pin):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.HIGH)


GPIO.setmode(GPIO.BCM)
try:
    while True:
        pin_out_float(21)
        time.sleep(1)
        #pin_out_low(21)
        time.sleep(1)
except KeyboardInterrupt:
    print('Closing...')
finally:
    GPIO.cleanup()
