import cv2
import time

nframes = 10
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1316)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 987)
cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)
cap.set(cv2.CAP_PROP_FPS, 20)
cap.set(cv2.CAP_PROP_CONVERT_RGB, 1)
frame_captured, image = cap.read()
time.sleep(2)
start_time = time.time()
for i in range(nframes):
    frame_captured, image = cap.read()
    print(image.shape)
stop_time = time.time()
fram_per_sec = nframes/(stop_time - start_time)
print('Frames per second rate: ' + str(fram_per_sec))
