import time
import cv2
import numpy
import threading

class screen():
    def __init__(self):
        self.white_screen = self.gen_basic_screen((255,255,255))
        self.yellow_screen = self.gen_basic_screen((255,255,0))
        self.green_screen = self.gen_basic_screen((0,255,0))
        self.red_screen = self.gen_basic_screen((255,0,0))
        
        self.white_screen = self.write_double_text(self.white_screen,
                                                'APROXIME-SE',
                                                'DA CAMERA',
                                                (0,0,0))
        self.yellow_screen = self.write_single_text(self.yellow_screen,
                                                    'ANALISANDO',
                                                    (0,0,0))
        self.red_screen = self.write_single_text(self.red_screen,
                                                 'DESCONHECIDO',
                                                    (255,255,255))
        
        window_names = ['white_window', 'yellow_window', 'green_window', 'red_window']
        for window_name in window_names:
            cv2.namedWindow(window_name)
            cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    def gen_basic_screen(self, color):
        blank_screen = numpy.zeros((1024,768,3), numpy.uint8)
        blank_screen[:,:] = color
        return blank_screen

    def write_single_text(self, img, text, color):
        font = cv2.FONT_HERSHEY_DUPLEX
        textsize = cv2.getTextSize(text, font, 8, 20)[0]
        x = int((img.shape[1] - textsize[0])/2)
        y = int((img.shape[0] - textsize[1])/2)
        cv2.putText(img, text, (x,y), font, 8, color, 20)
        return img

    def write_double_text(self, img, text1, text2, color):
        font = cv2.FONT_HERSHEY_DUPLEX
        textsize1 = cv2.getTextSize(text1, font, 8, 20)[0]
        textsize2 = cv2.getTextSize(text2, font, 8, 20)[0]

        factor = 100
        text_y1 = int(img.shape[0]/2 - factor)
        text_y2 = int(img.shape[0]/2 + factor + textsize2[1])
        text_x1 = int((img.shape[1] - textsize1[0])/2)
        text_x2 = int((img.shape[1] - textsize2[0])/2)

        cv2.putText(img, text1, (text_x1, text_y1), font, 8, (0, 0, 0), 20)
        cv2.putText(img, text2, (text_x2, text_y2), font, 8, (0, 0, 0), 20)
        return img
    
    def show_white_window(self):
        while white_flag:
            cv2.imshow('white_window', self.white_screen)
            cv2.waitKey(1)
        #cv2.destroyWindow('green_window')
        #cv2.destroyWindow('red_window')
        
    def show_yellow_window(self):
        while yellow_flag:
            cv2.imshow('yellow_window', self.yellow_screen)
            cv2.waitKey(1)
        #cv2.destroyWindow('white_window')
        
    def show_green_window(self, first_name, last_name):
        screen = self.write_double_text(self.green_screen, first_name, last_name)
        while green_flag:
            cv2.imshow('green_window', screen)
            cv2.waitKey(1)
        #cv2.destroyWindow('yellow_window')
        
    def show_red_window(self):
        while red_flag:
            cv2.imshow('red_window', self.red_screen)
            cv2.waitKey(1)
        #cv2.destroyWindow('yellow_window')
      
def show():
    while flag:
        while not white_flag:
            pass
        scr.show_white_window()
        while not yellow_flag:
            pass
        scr.show_yellow_window()
        while not green_flag:
            pass
        scr.show_green_window()
        
if __name__=='__main__':
    white_flag = False
    yellow_flag = False
    green_flag = False
    red_flag = False
    flag = True
    scr = screen()

    t = threading.Thread(target=show)
    t.start()
    
    time.sleep(2)
    white_flag = True
    time.sleep(2)
    yellow_flag = True
    white_flag = False
    time.sleep(2)
    red_flag = True
    yellow_flag = False
    time.sleep(2)
    red_flag = False
    flag = False

