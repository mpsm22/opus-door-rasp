from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2

camera = PiCamera()
camera.resolution = (1316, 988)
rawCapture = PiRGBArray(camera, size=(1316, 988))
camera.start_preview()
time.sleep(2)

start = time.time()

i = 0
for frame in camera.capture_continuous(rawCapture, format="rgb", use_video_port=True):
    image = frame.array #the frame will be stroed in the varible called image
    rawCapture.truncate(0)
    i += 1
    time.sleep(0.04)
    if i == 100:
        break
stop = time.time()
print((stop-start)/100)
    