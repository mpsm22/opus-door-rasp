import dlib
import cv2

detector = dlib.get_frontal_face_detector()
shape = dlib.shape_predictor('../../shape.dat')

img = cv2.imread('./Dominik_Garcia-Lorido_0001.jpg')
bnd = detector(img, 1)
bnd = bnd[0]
tl = bnd.tl_corner()
br = bnd.br_corner()
r = -0.1
a = (br.x-tl.x)*r/2
b = (br.y-tl.y)*r/2
tlx = int(tl.x - a)
tly = int(tl.y - b)
brx = int(br.x + a)
bry = int(br.y + b)
bnd = dlib.rectangle(tlx, tly, brx, bry)

shp = shape(img, bnd)
parts = shp.parts()
face_chip = dlib.get_face_chip(img, shp)

cv2.rectangle(img, (tlx, tly), (brx, bry), (255,0,0))
for p in parts:
    cv2.circle(img, (p.x, p.y), radius=2, color=(0,0,255), thickness=-1)

cv2.imshow('shape', img)
cv2.imshow('chip', face_chip)
if cv2.waitKey(0) == 27:
    exit()
