import socket
import cv2
import pickle

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostname(), 1234))
s.listen(5)

dic = {}
dic['img'] = cv2.imread('Marcos_Monteiro_0001.jpg')
dic['nam'] = 'Marcos Monteiro'

while True:
    # now our endpoint knows about the OTHER endpoint.
    clientsocket, address = s.accept()
    print(f"Connection from {address} has been established.")
    clientsocket.send(pickle.dumps(dic))
    clientsocket.close()
