import io
import time
import picamera
#import multiprocessing
from multiprocessing.pool import ThreadPool
#import threading
import cv2


threadn = cv2.getNumberOfCPUs()
pool = ThreadPool(processes = threadn)
Numberofimages=10

starttime=time.time()

#datawrite=DataWriter()
#datawrite.start()

def outputs():
    stream = io.BytesIO()
    for i in range(Numberofimages):
        yield stream
        
        #print(stream.getbuffer().nbytes)
        
        value = stream.getvalue()
        #print(stream.getbuffer().nbytes)
        #print(type(value))
        #print(len(value))
        
        stream.seek(0)
        #print(stream.getbuffer().nbytes)
        
        stream.truncate()
        #print(stream.getbuffer().nbytes)
        #print()

with picamera.PiCamera() as camera:
    camera.resolution = (1856, 1392)
    camera.framerate = 20
    camera.start_preview()
    time.sleep(2)
    
    start = time.time()
    camera.capture_sequence(outputs(),format='yuv',use_video_port=True)
    finish = time.time()
    print('Captured images at %.2ffps' % (Numberofimages / (finish - start)))
