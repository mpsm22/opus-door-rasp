from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/add-face', methods=['POST'])
def add_face():
    print('request received')
    name = request.json['name']
    print(name)
    photo = request.json['photo']
    print(photo)
    return jsonify({'delivered': True})

if __name__ == '__main__':
    app.run(debug=True)