import numpy as np
import cv2
from time import sleep

img = np.zeros((1080, 1920, 3), np.uint8)
img[:,:] = (0,255,0)

font = cv2.FONT_HERSHEY_DUPLEX
text1 = 'MARCOS'
text2 = 'MONTEIRO'

textsize1 = cv2.getTextSize(text1, font, 8, 20)[0]
textsize2 = cv2.getTextSize(text2, font, 8, 20)[0]

factor = 100
textY1 = int(img.shape[0]/2 - factor)
textY2 = int(img.shape[0]/2 + factor + textsize2[1])
textX1 = int((img.shape[1] - textsize1[0])/2)
textX2 = int((img.shape[1] - textsize2[0])/2)

cv2.putText(img, text1, (textX1, textY1), font, 8, (0, 0, 0), 20)
cv2.putText(img, text2, (textX2, textY2), font, 8, (0, 0, 0), 20)

cv2.namedWindow('gw')
cv2.setWindowProperty('gw', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('gw', img)
cv2.waitKey(0)
sleep(10)
