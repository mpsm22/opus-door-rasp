import numpy as np
import cv2
import time

cap = cv2.VideoCapture(0)
cap.set(3, 1920)
cap.set(4, 1088)
cap.set(cv2.CAP_PROP_BUFFERSIZE, 2)

while True:
        start = time.time()
        frame_captured, image = cap.read()
        stop = time.time()
        print(stop-start)
        time.sleep(0.5)
