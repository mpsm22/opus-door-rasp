import os
import time
from PyQt5.QtCore import QThread, pyqtSignal, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QWidget, QVBoxLayout
from PyQt5.QtGui import QPixmap
import threading

class Window(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setGeometry(0, 0, 1024, 768)
        self.setStyleSheet('background-color: red;')

class RekThread(QThread):
    def run(self):
        win.showFullScreen()
        time.sleep(2)
        win.setStyleSheet('background-color: yellow;')
        lab.setStyleSheet('font: bold 144pt; color: black')
        lab.setText('AMARELO')
        time.sleep(2)
        win.setStyleSheet('background-color: green;')
        lab.setStyleSheet('font: bold 144pt; color: white')
        lab.setText('VERDE')
        time.sleep(2)
        win.setStyleSheet('background-color: red;')
        lab.setStyleSheet('font: bold 144pt; color: white')
        lab.setText('VERMELHO')
    
    

if __name__=='__main__':
    app = QApplication([])
    
    win = Window()
    win.cent_widg = QWidget()
    win.setCentralWidget(win.cent_widg)
    lay = QVBoxLayout(win.cent_widg)
    lab = QLabel(win)
    lab.setAlignment(Qt.AlignCenter)
    lab.setStyleSheet('font: bold 144pt; color: white')
    lab.setText('VERMELHO')
    lay.addWidget(lab)
    
    thr1 = RekThread()
    thr1.finished.connect(thr1.start)

    thr1.start()
    app.exit(app.exec_())
    print('FLAG')
    