import cv2
from time import time

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1856)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1392)
cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)
cam.set(cv2.CAP_PROP_FPS, 20)

while True:
    ti = time()
    res, img = cam.read()
    tf = time()
    #print(tf-ti)
    #print(img.shape)
#cv2.imwrite('img1280x720.jpg', img)
    cv2.imshow('img', img)
    if cv2.waitKey(100) == 27:
       exit()