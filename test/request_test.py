import requests
import json
import cv2
import numpy as np

url = 'http://192.168.0.102:8080/get-faces'
response = requests.get(url)
if response.ok:
    json_data = response.json()
    photo = json_data[0]['photo']
    print(max(photo))
    print(min(photo))
    for i in range(len(photo)):
        if (photo[i] < 0):
            photo[i] = 128 - abs(photo[i]) + 128
    bytes_img = bytes(photo)
    nparr = np.frombuffer(bytes_img, np.uint8)
    img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    cv2.imshow('img', img_np)
    if cv2.waitKey(100) == 27:
        print('quit')