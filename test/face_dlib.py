import os
import dlib
import numpy as np
from skimage import io
import cv2
from datetime import datetime
import time

def to_dlib_rect(w, h):
    return dlib.rectangle(left=0, top=0, right=w, bottom=h)

def to_rect(dr):
    #  (x, y, w, h)
    return dr.left(), dr.top(), dr.right()-dr.left(), dr.bottom()-dr.top()

def face_detector_dlib(image):
    bounds = dlib_frontal_face_detector(image, 0) # second parameter is upsample; 1 or 2 will detect smaller faces. 0 performs similar to opencv with current parameters
    return list(map(lambda b: to_rect(b), bounds))


def get_face_encodings(face, bounds):
    faces_landmarks = [shape_predictor(face, face_bounds) for face_bounds in bounds]
    return [np.array(face_recognition_model.compute_face_descriptor(face, face_pose, 1)) for face_pose in faces_landmarks]

def load_face_encodings(faces_folder_path):
    image_filenames = filter(lambda x: x.endswith('.jpg'), os.listdir(faces_folder_path))
    image_filenames = sorted(image_filenames)
    person_names = [x[:-4] for x in image_filenames]
    print(person_names)

    full_paths_to_images = [faces_folder_path + x for x in image_filenames]
    known_faces = []

    for path_to_image in full_paths_to_images:
        face = io.imread(path_to_image)

        faces_bounds = dlib_frontal_face_detector(face, 0)

        if len(faces_bounds) != 1:
            print("Expected one and only one face per image: " + path_to_image + " - it has " + str(len(faces_bounds)))
            exit()

        face_bounds = faces_bounds[0]
        face_landmarks = shape_predictor(face, face_bounds)
        face_encoding = np.array(face_recognition_model.compute_face_descriptor(face, face_landmarks, 1))

        known_faces.append(face_encoding)
    return known_faces, person_names

def get_face_matches(known_faces, face):
    return np.linalg.norm(known_faces - face, axis=1)

def find_match(known_faces, person_names, face):
    matches = get_face_matches(known_faces, face) # get a list of True/False
    min_index = matches.argmin()
    min_value = matches[min_index]
    if min_value < 0.58:
        return person_names[min_index]+" ({0:.2f})".format(min_value)
    if min_value < 0.65:
        return person_names[min_index]+"?"+" ({0:.2f})".format(min_value)
    return 'Not Found'



dlib_frontal_face_detector = dlib.get_frontal_face_detector()
shape_predictor = dlib.shape_predictor('../../shape.dat')
face_recognition_model = dlib.face_recognition_model_v1('../../recognition.dat')
img = io.imread('./reference/Aaron_Peirsol_0001.jpg')
bnd = dlib_frontal_face_detector(img, 0)
face_encodings_in_image = get_face_encodings(img, bnd)
print(face_encodings_in_image)

        
    
