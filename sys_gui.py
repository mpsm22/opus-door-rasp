import door
from time import time
from PyQt5.QtCore import QThread, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QWidget, QVBoxLayout
from threading import Event

# não encontrei uma biblioteca mais leve para fazer a simples exibição dessas telas, porque as mais leves
# não permitiam uma execução assíncrona dessa tarefa
class Window(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setGeometry(0, 0, 1024, 600)
        self.cent_widg = QWidget()
        self.setCentralWidget(self.cent_widg)
        self.lay = QVBoxLayout(self.cent_widg)
        self.lab = QLabel(self)
        self.lab.setWordWrap(True)
        self.lab.setAlignment(Qt.AlignCenter)
        self.lay.addWidget(self.lab)
    
    def set_white(self):
        self.setStyleSheet('background-color: white;')
        self.lab.setStyleSheet('font: bold 96pt; color: black')
        self.lab.setText('APROXIME-SE')
        
    def set_yellow(self):
        self.setStyleSheet('background-color: yellow;')
        self.lab.setStyleSheet('font: bold 96pt; color: black')
        self.lab.setText('ANALISANDO')
        
    def set_green(self, name):
        self.setStyleSheet('background-color: green;')
        self.lab.setStyleSheet('font: bold 96pt; color: black')
        self.lab.setText(name.upper())
        
    def set_red(self):
        self.setStyleSheet('background-color: red;')
        self.lab.setStyleSheet('font: bold 96pt; color: white')
        self.lab.setText('DESCONHECIDO')

# evento é utilizado para chaveamento das telas; acredito que não seja a abordagem ideal
class WinThread(QThread):
    def run(self):
        win.show()
        while True:
            win.set_white()
            screen.clear()
            screen.wait()
            win.set_yellow()
            screen.clear()
            screen.wait()
            if rec.is_known:
                win.set_green(rec.name)
            else:
                win.set_red()
            screen.clear()
            screen.wait()

class RekThread(QThread):
    def run(self):
        clock = time()
        while True:
            while son.get_distance() > 150:
                clock = api.update_faces(clock, rec)
                continue
            screen.set()
            
            ti = time()
            rec_face = None
            for _ in range(3):
                print('[INFO] Face solicitada')
                det_face = soc.get()
                rec_face = rec.match_face(det_face['img'], det_face['bnd'])
                if rec_face is not None:
                    break
            tf = time()
            screen.set()
            
            # foram adicionados delays para compatibilizar o tempo no qual o relé fica acionado para
            # efeitos visuais na demonstração do TCC; até onde me lembro, a porta da Opus fica com a
            # fechadura aberta por apenas 1s; isso poderia ser feito de forma assíncrona
            print('[INFO] Tempo de reconhecimento: ' + str(tf-ti))
            if rec_face is not None:
                rly.switch_on()
                if rec_face['client']:
                    print('[INFO] Resultado do reconhecimento: '
                          + rec_face['name'] + ' (Cliente)')
                    client = api.get_client(rec_face['id'])
                    ntf.notify(rec_face['name'], client)
                    
                    
                    
                else:
                    print('[INFO] Resultado do reconhecimento: '
                          + rec_face['name'] + ' (Funcionario)')
                    door.sleep(7)
                rly.switch_off()
                api.add_log(rec_face['id'], det_face['img'])
                screen.set()
            else:
                print('[INFO] Resultado do reconhecimento: Desconhecido')
                door.sleep(3)
                api.add_log(-1, det_face['img'])
                screen.set()
            
          
if __name__ == '__main__':
    screen = Event()
    known = Event()
    
    soc = door.socketo()
    rec = door.recognizer()
    rly = door.relay()
    son = door.ultrasonic()
    api = door.api()
    ntf = door.notifier()

    init_faces = api.get_init_faces()
    rec.add_faces(init_faces)
    clock = time()
    print('[INFO] Programa principal inicializado')
    
    app = QApplication([])
    win = Window()
    
    thr1 = RekThread()
    thr1.finished.connect(thr1.start)
    thr2 = WinThread()
    thr2.finished.connect(thr2.start)
    
    thr1.start()
    thr2.start()
    app.exec_()