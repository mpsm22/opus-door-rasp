from flask import Flask, request, jsonify
from cv2 import imdecode, IMREAD_COLOR
import numpy
from json import dumps
import dlib
import requests

# procedimento que também ocorre no door.py; pode-se evitar essa repetição de código 
def format_bytes(img):
    for i in range(len(img)):
        if (img[i] < 0):
            img[i] = 128 - abs(img[i]) + 128
    return img

class recognizer():
    def __init__(self):
        self.recognition_model = dlib.face_recognition_model_v1('./recognition.dat')
        self.shape_predictor = dlib.shape_predictor('./shape.dat')
        self.face_detector = dlib.get_frontal_face_detector()

    def gen_face_desc(self, face_image):
        # é utilizado o detector da própria Dlib para localizar as faces nas imagens que vem do banco
        faces_bounds = self.face_detector(face_image)
        if len(faces_bounds) > 0:
            face_bounds = faces_bounds[0]
            # procedimentos que também ocorrem no door.py; pode-se evitar essa repetição de código 
            face_landmarks = self.shape_predictor(face_image, face_bounds)
            face_chip = dlib.get_face_chip(face_image, face_landmarks)
            face_descriptor = numpy.array(self.recognition_model.compute_face_descriptor(face_chip, 1))
            return face_descriptor
        else:
            return None

    def get_faces(self):
        url = 'http://192.168.0.105:8080/faces'
        response = requests.get(url)
        faces_web = response.json()
        
        faces_rasp = []
        for face_web in faces_web:
            if face_web['id'] == 30:
                continue
            name = face_web['name']
            photo = face_web['photo']
            photo = format_bytes(photo)
            img_byt = bytes(photo)
            img_arr = numpy.frombuffer(img_byt, numpy.uint8)
            img = imdecode(img_arr, IMREAD_COLOR)
            desc = self.gen_face_desc(img)
            
            if desc is not None:
                face_rasp = {}
                face_rasp['id'] = face_web['id']
                face_rasp['name'] = name
                # descritor é transformado em lista para que seja serializável
                face_rasp['desc'] = desc.tolist()
                face_rasp['client'] = face_web['client']
                faces_rasp.append(face_rasp)
        return faces_rasp


# este programa centraliza todas as requisições HTTP, mesmo aquelas feitas pela Rasp (no caso, pelo
# sys.py) que não precisam necessariamente passar pela API; não sei se essa é a melhor abordagem
app = Flask(__name__)
# note que as respostas HTTP muitas vezes não servem pra nada, não havendo confirmação na comunicação
# cliente-servidor; isso precisa ser aprimorado

@app.route('/get-web-faces', methods=['GET'])
def get_web_faces():
    print('[INFO] Solicitacao de todas as faces pela Raspberry')
    faces = rec.get_faces()
    obj = {'faces': faces}
    msg = dumps(obj)
    return msg

@app.route('/add-rasp-face', methods=['POST'])
def add_rasp_face():
    print('[INFO] Adicao de face pela Web')
    msg = request.json
    
    photo = format_bytes(msg['photo'])
    img_bytes = bytes(photo)
    img_nparr = numpy.frombuffer(img_bytes, numpy.uint8)
    img = imdecode(img_nparr, IMREAD_COLOR)
    desc = rec.gen_face_desc(img)
    
    face = {}
    face['id'] = msg['id']
    face['name'] = msg['name']
    face['desc'] = desc.tolist()
    face['client'] = msg['client']
    faces_to_add.append(face)
    
    if len(desc) > 0:
        return jsonify({'added': True})
    else:
        return jsonify({'added': False})
    
@app.route('/rem-rasp-face', methods=['POST'])
def rem_rasp_face():
    print('[INFO] Remocao de face pela Web')
    msg = request.json
    name = msg['name']
    names_to_rem.append(name)
    
    return jsonify({'added': True})

# API funciona como um buffer para as faces adicionadas / a serem removidas na Rasp (sys.py), fazendo
# o envio apenas quando solicitado; como foi comentado em door.py, isso poderia ser feito de forma
# assíncrona 
@app.route('/get-web-update', methods=['GET'])
def get_web_update():
    is_add_buf_empty = len(faces_to_add) > 0
    is_rem_buf_empty = len(names_to_rem) > 0
    obj = {'to_add': is_add_buf_empty,
            'add_faces' : faces_to_add,
            'to_rem': is_rem_buf_empty,
            'rem_names' : names_to_rem}
    msg = dumps(obj)
    
    faces_to_add.clear()
    names_to_rem.clear()
    return msg

@app.route('/get-client', methods=['GET'])
def get_client():
    print('[INFO] Solicitacao de dados de cliente pela Raspberry')
    param = request.args
    url = 'http://192.168.0.105:8080/client/' + param['id']
    resp = requests.get(url)
    obj = resp.json()
    return dumps(obj)
    
@app.route('/add-log', methods=['POST'])
def add_log():
    print('[INFO] Envio de registro para a Web')
    obj = request.json
    url = 'http://192.168.0.105:8080/log'
    requests.post(url, json=obj)
    return dumps('ok')
    

if __name__ == '__main__':
    rec = recognizer()
    # buffer para as faces a serem adicionadas / removidas
    faces_to_add = []
    names_to_rem = []
    app.run(host='0.0.0.0', debug=True)