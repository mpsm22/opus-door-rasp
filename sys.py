import door
from time import time

soc = door.socketo()
rec = door.recognizer()
rly = door.relay()
son = door.ultrasonic()
api = door.api()
ntf = door.notifier()

init_faces = api.get_init_faces()
rec.add_faces(init_faces)
clock = time()
print('[INFO] Programa principal inicializado')

while True:
    # a distância utilizada foi arbitrária
    while son.get_distance() > 150:
        # como dito em door.py, poderia ser feito de forma assíncrona
        clock = api.update_faces(clock, rec)
        continue
    
    ti = time()
    rec_face = None
    # são feitas 4 análises de faces por reconhecimento e, caso nenhuma delas tenha resultado positivo,
    # considera-se que a pessoa é desconhecida; suspeito que essa não seja a abordagem ideal
    for _ in range(4):
        print('[INFO] Face solicitada')
        det_face = soc.get()
        rec_face = rec.match_face(det_face['img'], det_face['bnd'])
        if rec_face is not None:
            break
    
    tf = time()
    rly.switch_on()
    door.sleep(1)
    rly.switch_off()
    print('[INFO] Tempo de reconhecimento: ' + str(tf-ti))
    if rec_face is not None:
        if rec_face['client']:
            print('[INFO] Resultado do reconhecimento: '
                  + rec_face['name'] + ' (Cliente)')
            client = api.get_client(rec_face['id'])
            ntf.notify(rec_face['name'], client)
        else:
            print('[INFO] Resultado do reconhecimento: '
                  + rec_face['name'] + ' (Funcionario)')
        api.add_log(rec_face['id'], det_face['img'])
        door.sleep(2)
    else:
        print('[INFO] Resultado do reconhecimento: Desconhecido')
        api.add_log(-1, det_face['img'])
        door.sleep(2)
        

